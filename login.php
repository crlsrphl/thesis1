<?php include 'php/header3.php'?>

<?php
session_start();

include ('phpconnections/connection.php');
include ('phpconnections/function.php');

if(loggedIn()){
  header("Location:index2.php");
  exit();
} 

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $email = $conn->real_escape_string($_POST['email']);
  $password = $conn->real_escape_string($_POST['password']);

  $result = $conn->query("SELECT * FROM user WHERE uemail='$email' AND upass='$password'") or die(mysqli_error($conn));
  $row = mysqli_fetch_array($result);


  if ($email == $row['uemail'] && $password == $row['upass']) {
    if ($row['vstatus'] == 1) {
      $_SESSION['uemail'] = $email;
      header("Location:index2.php");
      exit();
    }else{
      header("Location:index.php?err=" . urlencode("The user account is not activated!"));
      exit();
    }
  }else{
    header("Location:index.php?err=" . urlencode("Wrong Email or Password!"));
    exit();
  }
}

?>

<section id="projects" class="projects-section bg-light">
      <div class="container">

        <!-- Featured Project Row -->
        <div class="row align-items-center no-gutters mb-4 mb-lg-5">

          <div class="col-xl-12 col-lg-5">
            <div class="text-center text-lg-center">
              <h4 class="text-uppercase m-0">Welcome to our Family!!!</h4><br>
              <?php include 'forms/loginforms.php'?>
              <p><a href="REGISTER.php">Don't have yet an account?</a></p>
            </div>
          </div>
        </div>
       </div> 
</section>


<?php include 'php/footer.php'?>

</body>
</html>