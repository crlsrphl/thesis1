<?php include 'php/header6.php'?>

<section id="projects" class="projects-section bg-light">
      <div class="container">

        <div class="column no-gutters mb-10 mb-lg-10">

          <h2>Welcome Admin!!!</h2><br><br>
          <div class="featured-text col-xl-10 col-lg-10">
            <p><a class="nav-link" href="reservationreport.php">Reservation Report</a>
            <a class="nav-link" href="checkbooking.php">Check Booking</a>
            <a class="nav-link" href="customerlist.php">Customer List</a>
            <a class="nav-link" href="empadmin.php">The Admins</a>
           
          </div> <br>

           <div class="col-md-12 mb-9 mb-md-0">
            <div id="slider">
              <figure>
              <img src="img/pictures-large/20.jpg">
              <img src="img/pictures-large/61.jpg">
              <img src="img/pictures-large/44.jpg">
              <img src="img/pictures-large/93.jpg">
              <img src="img/pictures-large/81.jpg">
              <img src="img/pictures-large/21.jpg">
              <img src="img/pictures-large/3.jpg">
              <img src="img/pictures-large/32.jpg">
              <img src="img/pictures-large/16.jpg">
              <img src="img/pictures-large/39.jpg">
              </figure>
            </div> <br>

           </div>

        </div>

      </div>
</section>

<footer class="bg-black"></footer>  

<!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>
</html>