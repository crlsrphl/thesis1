<?php include 'php/header2.php'?>


<section id="projects" class="projects-section bg-light">
      <div class="container">

        <!-- Featured Project Row -->
        <div class="row align-items-center no-gutters mb-4 mb-lg-5">
          <div class="featured-text col-xl-8 col-lg-7">
            <h1 class="text-uppercase m-0">Studio 4104 Services</h1><br><br>

            <h3>Studio Rentals</h3><br>
            <h5>Package 1</h5><br>
            <h6>* P500/hour (Minimum of 3 hours rental. Exceeding hours will be charged P500)</h6><br>

            <h5>Package 2</h5><br>
            <h6>* P3000 (Minimum of 3 hours rental. Exceeding hours will be charged P500)<br>* 20 Edited Photos</h6><br><br>

            <h3>Wedding Packages</h3><br>
            <h5>Wedding Package 1</h5><br>
            <h6>* P20,000 <br>* Prenup and Wedding shoot <br>* 2 Photographers <br>* 1 Videographer</h6><br><br>   
            <h5>Wedding Package 2</h5><br>
            <h6>* P25,000 <br>* Same Day Edit shoot <br>* Album <br>* Prenup shoot <br>* 10 pages Guest Book <br>* Slideshow <br>* Teaser of video prenup</h6><br><br>

            <h5>Wedding Package 3</h5><br>
            <h6>* P70,000 <br>* Same Day Edit shoot <br>* Album with 20 pages <br>* 11x14 Frame <br>* 10 pages Guest Book <br>* Slideshow <br>* Teaser of video prenup</h6><br><br>

            <h3>Photobooth</h3><br>
            <h6>* P3,500 <br>* 2 hours <br>* 1 Photobooth</h6><br><br>

            <h3>Birthday/Christening Packages</h3><br>
            <h5>Birthday/Christening Package 1</h5><br>
            <h6>* P8,000 <br>* Birthday/Christening Shoot</h6><br>

            <h5>Birthday/Christening Package 2</h5><br>
            <h6>* P15,000 <br>* Photo and Video Coverage</h6><br>

            <h5>Birthday/Christening Package 3</h5><br>
            <h6>* P20,000 <br>* Photo and Video Coverage <br>* Photo album</h6><br><br>

            <h3>Makeup Stylist</h3><br>
            <h6>* P1,500 per hour</h6><br><br>

            <h3>Debut Package</h3><br>
            <h5>Debut Package 1</h5><br>
            <h6>* 22,000php <br>* 1 Photographer <br>* 1 Videographer <br>* Pre-shoot(Predebut) <br>* Unlimited High Resolution photos <br>* Cinematic Highlights(Mtv style) <br>* Photos & edited video will be saved in USB(Free) <br>* 20 pages, 8x10 Magnetic Coffee Table Album(with leather box) <br>* Free USB Drive</h6><br>

            <h5>Debut Package 2</h5><br>
            <h6>* 30,000php <br>* 2 Photographers <br>* 1 Videographer <br>* Pre-shoot(Predebut) <br>* AVP Presentation(Growing up) <br>* Unlimited High Resolution photos <br>* Cinematic Highlights(Mtv style) <br>* Photos and edited video will be saved in USB <br>* 20 pages, 8x10 Magnetic Coffee Table Album(with leather box) <br>* Free USB Drive </h6><br>

            <h5>Debut Package 3</h5><br>
            <h6>* 35,000 <br>* 2 Photographers <br>* 2 Videographers <br>* Pre-shoot(predebut) <br>* AVP Presentation(Growing up) <br>* Cinematic Highlights(Mtv style) <br>* Unlimited High Resolution photos <br>* Photos & edited videos will be saved in USB <br>* 20 pages, 8x10 Magnetic Coffee Table Album(with leather box) <br>* Free USB Drive </h6><br><br>

            <h3>Pre Nuptial Package</h3><br>
            <h5>Pre Nuptial Package 1 (Photo Only)</h5><br>
            <h6>* 15,000php <br>* 2 Photographers <br>* 20 pages, 8x10 Coffee Table Album(with leather box) <br>* 50pcs Enhanced photos <br>* Free USB</h6><br>

            <h5>Pre Nuptial Package 2 (Video Only)</h5><br>
            <h6>* 15,000php <br>* 2 Videographers <br>* 1 Edited Video(mtv style) OR Save the date video <br>* Free USB</h6><br><br>
   
          </div>
          </div>

        </div>
      </section>

<?php include 'php/footer.php'?>

</body>

</html>      